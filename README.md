# mhx2

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A crossplatform embeddable web server, including a standalone web server with CGI support.

Embed a web server into your C++ project, or use the standalone server to run .bat files as CGI!

The socket loop is `select()`-based and supports singlethreaded or thread-pooled usage using either Win32 or POSIX threads.
